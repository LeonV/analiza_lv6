﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IgraVijesala
{
    public partial class Form1 : Form
    {
        string secretWord;
        int n = 0;
        string path;
        public Form1()
        {
            path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\data.txt";

            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null){
                    n++;
                }
            }
            InitializeComponent();
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path)){
                labelLetters.Text = "";
                labelHiddenWord.Text = "";
                labelTries.Text = "7";
                Random random = new Random(DateTime.Now.Millisecond);
                int r = random.Next(1, n);
                for (int i = 0; i < r; i++)
                    secretWord = reader.ReadLine().ToUpper();
                for (int i = 0; i < secretWord.Length; i++)
                    labelHiddenWord.Text = labelHiddenWord.Text + "#";
            }
        }
        private void buttonGuess_Click(object sender, EventArgs e)
        {
            String Letter = tbLetter.Text.ToString();
            StringBuilder temp = new StringBuilder(labelHiddenWord.Text.ToString());
            if (labelHiddenWord.Text == secretWord)
                MessageBox.Show("Pogodili ste riječ!", "Pobjeda");
            else {
                if (labelTries.Text == "0")
                    MessageBox.Show("Nemate više pokušaja!", "Kraj Igre");
                else {
                    if (labelLetters.Text.Contains(Letter))
                        MessageBox.Show("Vec ste upisali to slovo", "Greška");
                    else {
                        bool contains = false;
                        labelLetters.Text = labelLetters.Text + Letter + " ";
                        for (int i = 0; i < secretWord.Length; i++)
                        {
                            if (secretWord[i] == Letter[0])
                            {
                                temp[i] = Letter[0];
                                contains = true;
                            }
                        }
                        if (!contains)
                        {
                            labelTries.Text = (int.Parse(labelTries.Text) - 1).ToString();
                            if (labelTries.Text == "0")
                                MessageBox.Show("Nemate više pokušaja!", "Kraj Igre");
                        }
                        labelHiddenWord.Text = temp.ToString();
                        if (labelHiddenWord.Text.ToString() == secretWord)
                            MessageBox.Show("Pogodili ste riječ!", "Pobjeda");
                    }
                }
            }
        }
        private void buttonNewGame_Click(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                labelLetters.Text = "";
                labelHiddenWord.Text = "";
                labelTries.Text = "7";
                Random random = new Random(DateTime.Now.Millisecond);
                int r = random.Next(1, n);
                for (int i = 0; i < r; i++)
                    secretWord = reader.ReadLine().ToUpper();
                for (int i = 0; i < secretWord.Length; i++)
                    labelHiddenWord.Text = labelHiddenWord.Text + "#";
            }
        }
        private void buttonShowWord_Click(object sender, EventArgs e)
        {
            labelHiddenWord.Text = secretWord+" ";
            labelTries.Text = "0";
        }
    }
}
