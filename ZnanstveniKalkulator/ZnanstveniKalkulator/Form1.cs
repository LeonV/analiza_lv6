﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZnanstveniKalkulator
{
    public partial class Form1 : Form
    {
        double value = 0;
        double ans = 0;
        string operation = "";
        bool operation_click = false;
        public Form1() {
            InitializeComponent();
        }
        private void textBox1_TextChanged(object sender, EventArgs e){

        }
        private void buttonDot_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "") || operation_click )
            {
                textBox1.Text = "0";
                operation_click = false;
            }
            if (!(textBox1.Text.Contains(',')))
            {
                textBox1.Text = textBox1.Text + ",";
            }
        }
        private void buttonNegate_Click(object sender, EventArgs e)
        {
            textBox1.Text = (double.Parse(textBox1.Text)*(-1)).ToString();
        }
        private void button0_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "0") || (operation_click))
                textBox1.Clear();
            textBox1.Text = textBox1.Text + "0";
            operation_click = false;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "0") || (operation_click))
                textBox1.Clear();
            textBox1.Text = textBox1.Text + "1";
            operation_click = false;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "0") || (operation_click))
                textBox1.Clear();
            textBox1.Text = textBox1.Text + "2";
            operation_click = false;
        }
        private void button3_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "0") || (operation_click))
                textBox1.Clear();
            textBox1.Text = textBox1.Text + "3";
            operation_click = false;
        }
        private void button4_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "0") || (operation_click))
                textBox1.Clear();
            textBox1.Text = textBox1.Text + "4";
            operation_click = false;
        }
        private void button5_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "0") || (operation_click) )
                textBox1.Clear();
            textBox1.Text = textBox1.Text + "5";
            operation_click = false;
        }
        private void button6_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "0") || (operation_click))
                textBox1.Clear();
            textBox1.Text = textBox1.Text + "6";
            operation_click = false;
        }
        private void button7_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "0") || (operation_click))
                textBox1.Clear();
            textBox1.Text = textBox1.Text + "7";
            operation_click = false;
        }
        private void button8_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "0") || (operation_click))
                textBox1.Clear();
            textBox1.Text = textBox1.Text + "8";
            operation_click = false;
        }
        private void button9_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text == "0") || (operation_click))
                textBox1.Clear();
            textBox1.Text = textBox1.Text + "9";
            operation_click = false;
        }
        private void buttonC_Click(object sender, EventArgs e)
        {
            ans = 0;
            textBox1.Text = "0";
            label.Text = "";
            operation_click = false;
        }
        private void buttonCE_Click(object sender, EventArgs e)
        {
            textBox1.Text = "0";
        }
        private void buttonAns_Click(object sender, EventArgs e)
        {
            textBox1.Text = ans.ToString();
            operation_click = false;
        }
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            value = double.Parse(textBox1.Text);
            operation = "+";
            label.Text = value.ToString() + " + ";
            operation_click = true;
        }
        private void buttonSub_Click(object sender, EventArgs e)
        {
            value = double.Parse(textBox1.Text);
            operation = "-";
            label.Text = value.ToString() + " - ";
            operation_click = true;
        }
        private void buttonMultiply_Click(object sender, EventArgs e)
        {
            value = double.Parse(textBox1.Text);
            operation = "*";
            label.Text = value.ToString() + " * ";
            operation_click = true;
        }
        private void buttonDiv_Click(object sender, EventArgs e)
        {
            value = double.Parse(textBox1.Text);
            operation = "/";
            label.Text = value.ToString() + " / ";
            operation_click = true;
        }
        private void buttonEqual_Click(object sender, EventArgs e)
        {
            if (!operation_click)
            {
                switch (operation)
                {
                    case "+":
                        textBox1.Text = (value + Double.Parse(textBox1.Text)).ToString();
                        break;
                    case "-":
                        textBox1.Text = (value - Double.Parse(textBox1.Text)).ToString();
                        break;
                    case "*":
                        textBox1.Text = (value * Double.Parse(textBox1.Text)).ToString();
                        break;
                    case "/":
                        textBox1.Text = (value / Double.Parse(textBox1.Text)).ToString();
                        break;
                    default:
                        break;
                }
                operation_click = true;
                label.Text = "";
                operation = "";
            }
            ans = Double.Parse(textBox1.Text);
        }
        private void buttonSquare_Click(object sender, EventArgs e)
        {
            value = double.Parse(textBox1.Text);
            textBox1.Text = (value * value).ToString();
            ans = Double.Parse(textBox1.Text);
            label.Text = "";
            operation = "";
            operation_click = true;
        }
        private void buttonCube_Click(object sender, EventArgs e)
        {
            value = double.Parse(textBox1.Text);
            textBox1.Text = (value * value * value).ToString();
            ans = Double.Parse(textBox1.Text);
            label.Text = "";
            operation = "";
            operation_click = true;
        }
        private void buttonSqrt_Click(object sender, EventArgs e)
        {
            value = double.Parse(textBox1.Text);
            if (value < 0)
                MessageBox.Show("Unos nije valjan", "Pogreška!");
            else
            {
                textBox1.Text = (Math.Sqrt(value)).ToString();
                ans = Double.Parse(textBox1.Text); 
            }
            label.Text = "";
            operation = "";
            operation_click = true;
        }
        private void buttonInv_Click(object sender, EventArgs e)
        {
            value = double.Parse(textBox1.Text);
            if (value == 0)
                MessageBox.Show("Unos nije valjan", "Pogreška!");
            else { 
                textBox1.Text = (1/value).ToString();
                ans = Double.Parse(textBox1.Text);
            }
            label.Text = "";
            operation = "";
            operation_click = true;
        }
        private void buttonSin_Click(object sender, EventArgs e)
        {
            value = double.Parse(textBox1.Text);
            textBox1.Text = (Math.Sin(value)).ToString();
            ans = Double.Parse(textBox1.Text);
            label.Text = "";
            operation = "";
            operation_click = true;
        }
        private void buttonCos_Click(object sender, EventArgs e)
        {
            value = double.Parse(textBox1.Text);
            textBox1.Text = (Math.Cos(value)).ToString();
            ans = Double.Parse(textBox1.Text);
            label.Text = "";
            operation = "";
            operation_click = true;
        }
        private void buttonTan_Click(object sender, EventArgs e)
        {
            value = double.Parse(textBox1.Text);
            textBox1.Text = (Math.Tan(value)).ToString();
            ans = Double.Parse(textBox1.Text);
            label.Text = "";
            operation = "";
            operation_click = true;
        }
        private void buttonCotan_Click(object sender, EventArgs e)
        {
            value = double.Parse(textBox1.Text);
            textBox1.Text = (1/(Math.Tan(value))).ToString();
            ans = Double.Parse(textBox1.Text);
            label.Text = "";
            operation = "";
            operation_click = true;
        }
        private void buttonLog_Click(object sender, EventArgs e)
        {
            value = double.Parse(textBox1.Text);
            if (value <= 0)
                MessageBox.Show("Unos nije valjan", "Pogreška!");
            else{
                textBox1.Text = (Math.Log10(value)).ToString();
                ans = Double.Parse(textBox1.Text);
            }
            label.Text = "";
            operation = "";
            operation_click = true;
        }
        private void buttonLn_Click(object sender, EventArgs e)
        {
            value = double.Parse(textBox1.Text);
            if (value <= 0)
                MessageBox.Show("Unos nije valjan", "Pogreška!");
            else{
                textBox1.Text = (Math.Log(value)).ToString();
                ans = Double.Parse(textBox1.Text);
            }
            label.Text = "";
            operation = "";
            operation_click = true;
        }
    }
}
