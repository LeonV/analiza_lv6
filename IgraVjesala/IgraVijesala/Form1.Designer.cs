﻿namespace IgraVijesala
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonNewGame = new System.Windows.Forms.Button();
            this.labelText = new System.Windows.Forms.Label();
            this.labelTries = new System.Windows.Forms.Label();
            this.tbLetter = new System.Windows.Forms.TextBox();
            this.buttonGuess = new System.Windows.Forms.Button();
            this.labelLetters = new System.Windows.Forms.Label();
            this.labelHiddenWord = new System.Windows.Forms.Label();
            this.buttonShowWord = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonNewGame
            // 
            this.buttonNewGame.Location = new System.Drawing.Point(12, 12);
            this.buttonNewGame.Name = "buttonNewGame";
            this.buttonNewGame.Size = new System.Drawing.Size(75, 23);
            this.buttonNewGame.TabIndex = 0;
            this.buttonNewGame.Text = "Nova igra";
            this.buttonNewGame.UseVisualStyleBackColor = true;
            this.buttonNewGame.Click += new System.EventHandler(this.buttonNewGame_Click);
            // 
            // labelText
            // 
            this.labelText.AutoSize = true;
            this.labelText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelText.Location = new System.Drawing.Point(9, 45);
            this.labelText.Name = "labelText";
            this.labelText.Size = new System.Drawing.Size(156, 17);
            this.labelText.TabIndex = 1;
            this.labelText.Text = "Preostali broj pokušaja:";
            // 
            // labelTries
            // 
            this.labelTries.AutoSize = true;
            this.labelTries.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTries.Location = new System.Drawing.Point(180, 45);
            this.labelTries.Name = "labelTries";
            this.labelTries.Size = new System.Drawing.Size(16, 17);
            this.labelTries.TabIndex = 2;
            this.labelTries.Text = "0";
            // 
            // tbLetter
            // 
            this.tbLetter.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbLetter.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLetter.Location = new System.Drawing.Point(12, 65);
            this.tbLetter.MaxLength = 1;
            this.tbLetter.Name = "tbLetter";
            this.tbLetter.Size = new System.Drawing.Size(34, 41);
            this.tbLetter.TabIndex = 3;
            // 
            // buttonGuess
            // 
            this.buttonGuess.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGuess.Location = new System.Drawing.Point(52, 65);
            this.buttonGuess.Name = "buttonGuess";
            this.buttonGuess.Size = new System.Drawing.Size(75, 41);
            this.buttonGuess.TabIndex = 4;
            this.buttonGuess.Text = "Pogodi!";
            this.buttonGuess.UseVisualStyleBackColor = true;
            this.buttonGuess.Click += new System.EventHandler(this.buttonGuess_Click);
            // 
            // labelLetters
            // 
            this.labelLetters.AutoSize = true;
            this.labelLetters.Location = new System.Drawing.Point(12, 121);
            this.labelLetters.Name = "labelLetters";
            this.labelLetters.Size = new System.Drawing.Size(34, 13);
            this.labelLetters.TabIndex = 5;
            this.labelLetters.Text = "Slova";
            // 
            // labelHiddenWord
            // 
            this.labelHiddenWord.AutoSize = true;
            this.labelHiddenWord.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHiddenWord.Location = new System.Drawing.Point(133, 76);
            this.labelHiddenWord.Name = "labelHiddenWord";
            this.labelHiddenWord.Size = new System.Drawing.Size(63, 24);
            this.labelHiddenWord.TabIndex = 6;
            this.labelHiddenWord.Text = "Word*";
            // 
            // buttonShowWord
            // 
            this.buttonShowWord.Location = new System.Drawing.Point(183, 12);
            this.buttonShowWord.Name = "buttonShowWord";
            this.buttonShowWord.Size = new System.Drawing.Size(75, 23);
            this.buttonShowWord.TabIndex = 7;
            this.buttonShowWord.Text = "Prikaži riječ";
            this.buttonShowWord.UseVisualStyleBackColor = true;
            this.buttonShowWord.Click += new System.EventHandler(this.buttonShowWord_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(270, 143);
            this.Controls.Add(this.buttonShowWord);
            this.Controls.Add(this.labelHiddenWord);
            this.Controls.Add(this.labelLetters);
            this.Controls.Add(this.buttonGuess);
            this.Controls.Add(this.tbLetter);
            this.Controls.Add(this.labelTries);
            this.Controls.Add(this.labelText);
            this.Controls.Add(this.buttonNewGame);
            this.Name = "Form1";
            this.Text = "Igra vješala";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonNewGame;
        private System.Windows.Forms.Label labelText;
        private System.Windows.Forms.Label labelTries;
        private System.Windows.Forms.TextBox tbLetter;
        private System.Windows.Forms.Button buttonGuess;
        private System.Windows.Forms.Label labelLetters;
        private System.Windows.Forms.Label labelHiddenWord;
        private System.Windows.Forms.Button buttonShowWord;
    }
}

